package service;

import domain.Book;
import repository.entity.BookRepository;
import service.interfaces.IBookService;

public class BookService implements IBookService {
    private BookRepository bookRepo;

    public BookService(){
        bookRepo = new BookRepository();
    }

    @Override
    public void add(Book book) {
        bookRepo.add(book);
    }

    @Override
    public void update(Book book) {
        bookRepo.update(book);
    }

    @Override
    public void remove(Book book) {
        bookRepo.remove(book);
    }

    @Override
    public Book getEntityBtId(int id) {
            return bookRepo.getBookById(id);
    }





}
