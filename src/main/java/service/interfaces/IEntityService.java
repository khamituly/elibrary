package service.interfaces;


public interface IEntityService<T> {

        void add(T entity);
        void update(T entity);
        void remove(T entity) ;
        T getEntityBtId(int id);


}
