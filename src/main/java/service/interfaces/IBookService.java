package service.interfaces;

import domain.Book;


public interface IBookService extends IEntityService<Book> {


    @Override
    default void add(Book entity) {

    }

    @Override
    default void update(Book entity) {

    }

    @Override
    default void remove(Book entity) {

    }

    @Override
    default Book getEntityBtId(int id) {
        return null;
    }




}
