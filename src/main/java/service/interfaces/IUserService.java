package service.interfaces;

import domain.User;


public interface IUserService extends IEntityService<User>{


    @Override
    default void add(User entity) {

    }

    @Override
    default void update(User entity) {

    }

    @Override
    default void remove(User entity) {

    }

    @Override
    default User getEntityBtId(int id) {
        return null;
    }




}
