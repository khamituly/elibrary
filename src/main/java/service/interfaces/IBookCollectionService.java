package service.interfaces;

import domain.Book;

import java.util.LinkedList;

public interface IBookCollectionService {
    LinkedList<Book> getCollectionByID(int id);

    void addToCollection(int userId,int bookId);

    void removeFromCollection(int userId, int bookId);

}
