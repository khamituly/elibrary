package service;

import domain.User;
import repository.entity.UserRepository;
import service.interfaces.IUserService;



public class UserService implements IUserService {
    private UserRepository userRepo;

    public UserService(){
        userRepo = new UserRepository();
    }

    @Override
    public void add(User user) {
        userRepo.add(user);
    }

    @Override
    public void update(User user) {
        userRepo.update(user);
    }

    @Override
    public void remove(User entity) {
        userRepo.remove(entity);
    }

    @Override
    public User getEntityBtId(int id) {
        return userRepo.getUserById(id);
    }




}
