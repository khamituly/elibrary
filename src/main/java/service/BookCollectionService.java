package service;

import domain.Book;
import repository.entity.BookCollectionRepository;
import service.interfaces.IBookCollectionService;

import java.util.LinkedList;

public class BookCollectionService implements IBookCollectionService {
    private BookCollectionRepository bcsRepo;

    public BookCollectionService() {
        bcsRepo = new BookCollectionRepository();
    }

    @Override
    public LinkedList<Book> getCollectionByID(int userId) {
        return bcsRepo.getlistOfBook(userId);
    }

    @Override
    public void addToCollection(int userId, int bookId) {
        bcsRepo.add(userId, bookId);
    }

    @Override
    public void removeFromCollection(int userId, int bookId) {
        bcsRepo.remove(userId, bookId);
    }
}
