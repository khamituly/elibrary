package controllers;

import Filters.customAnnotations.JWTTokenNeeded;
import Filters.customAnnotations.OnlyAdmin;
import controllers.interfaces.Controllers;
import domain.User;
import org.glassfish.jersey.media.multipart.FormDataParam;
import service.UserService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



@Path("users")
public class UserController implements Controllers {
    private UserService userService;

    public UserController() {
        this.userService = new UserService();
    }

    @GET
    @Override
    public String welcome() {
        return "Welcome to eLibrary v1.0";
    }

    @JWTTokenNeeded
    @GET
    @Path("/{id}")
    public Response getUserById(@PathParam("id") int id) {
        User user;
        try {
            user = userService.getEntityBtId(id);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.ACCEPTED)
                    .entity("This user cannot be created").build();
        }

        if (user == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("User does not exists!")
                    .build();
        } else {
            return Response
                    .status(Response.Status.OK)
                    .entity(user)
                    .build();
        }
    }

    @OnlyAdmin
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/add")
    public Response addUSer(@FormDataParam("firstname") String firstname,
                            @FormDataParam("lastname") String lastname,
                            @FormDataParam("username") String username,
                            @FormDataParam("password") String password,
                            @FormDataParam("role") String role) {

        try {
            userService.add(new User(firstname, lastname, username, password, role));
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response
                .status(Response.Status.CREATED)
                .entity("User Created Succesfully!")
                .build();
    }


    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/update")
    public Response updateUser(User user, @Context ContainerRequestContext requestContext) {

        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(user.getUsername())) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        try {
            userService.update(user);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.OK)
                .entity("User updated successfully!")
                .build();
    }


    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/remove")
    public Response removeUser(@FormDataParam("userId") int userId,
                               @FormDataParam("username") String username,
                               @Context ContainerRequestContext requestContext) {
        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(username)) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        User user = userService.getEntityBtId(userId);
        userService.remove(user);

        return Response.
                status(Response.Status.OK)
                .entity("User deleted successfully!")
                .build();
    }
}
