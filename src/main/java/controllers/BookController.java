package controllers;

import Filters.customAnnotations.JWTTokenNeeded;
import Filters.customAnnotations.OnlyAdmin;
import controllers.interfaces.Controllers;
import domain.Book;
import org.glassfish.jersey.media.multipart.FormDataParam;
import service.BookService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



@Path("books")
public class BookController implements Controllers{
    private BookService bookService;

    public BookController(){
        this.bookService = new BookService();
    }

    @GET
    @Override
    public String welcome() {
        return "Welcome to eLibrary v1.0";
    }

    @JWTTokenNeeded
    @GET
    @Path("/{id}")
    public Response getBookById(@PathParam("id") int id){
            Book book = bookService.getEntityBtId(id);
            if(book == null){
                return Response.status(Response.Status.NOT_FOUND)
                        .entity("Book does not exists!")
                        .build();
            }else{
                return Response
                        .status(Response.Status.OK)
                        .entity(book)
                        .build();
            }
    }

    @OnlyAdmin
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("add")
    public Response addBook(@FormDataParam("name") String name,
                            @FormDataParam("author") String author){
        bookService.add(new Book(name,author));
        return Response
                .status(Response.Status.CREATED)
                .entity("Book created successfully!")
                .build();
    }

    @OnlyAdmin
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/update")
    public Response updateUser(Book book) {

        try {
            bookService.update(book);
        } catch (ServerErrorException ex) {
            return Response
                    .serverError().entity(ex.getMessage()).build();
        } catch (BadRequestException ex) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity(ex.getMessage()).build();
        }

        return Response.
                status(Response.Status.OK)
                .entity("Book updated successfully!")
                .build();
    }

    @OnlyAdmin
    @POST
    @Path("/remove/{id}")
    public Response removeUser(@PathParam("id" ) int bookId){

        Book book = bookService.getEntityBtId(bookId);
        bookService.remove(book);

        return Response.
                status(Response.Status.OK)
                .entity("Book deleted successfully!")
                .build();
    }
}
