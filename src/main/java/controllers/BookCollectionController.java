package controllers;

import Filters.customAnnotations.JWTTokenNeeded;
import controllers.interfaces.Controllers;
import domain.Book;
import org.glassfish.jersey.media.multipart.FormDataParam;
import service.BookCollectionService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;

@Path("collection")
public class BookCollectionController implements Controllers {
    private BookCollectionService bcs;

    public BookCollectionController(){
        bcs = new BookCollectionService();
    }

    @GET
    @Override
    public String welcome() {
        return "welcome to eLibrary v1.0";
    }

    @JWTTokenNeeded
    @POST
    @Path("/{userId}")
    public Response getCollectionOfBook(@PathParam("userId") int userId){

        LinkedList<Book> listOfBooks = bcs.getCollectionByID(userId);
        if(listOfBooks == null){
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("User does not collection yet!")
                    .build();
        }else{
            return Response
                    .status(Response.Status.OK)
                    .entity(listOfBooks)
                    .build();
        }
    }

    @JWTTokenNeeded
    @POST
    @Path("/add")
    public Response addBookToCollection(@FormDataParam("userId") int userId,
                                        @FormDataParam("bookId") int bookId,
                                        @FormDataParam("username") String username,
                                        @Context ContainerRequestContext requestContext){

        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(username)) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        bcs.addToCollection(userId,bookId);
        return Response
                .status(Response.Status.CREATED)
                .entity("Book added successfully!")
                .build();
    }

    @JWTTokenNeeded
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/remove")
    public Response removeUser(@FormDataParam("userId") int userId,
                               @FormDataParam("bookId") int bookId,
                               @FormDataParam("username") String username,
                               @Context ContainerRequestContext requestContext){

        if (!requestContext.getSecurityContext().isUserInRole("admin") &&
                !requestContext.getSecurityContext().getUserPrincipal().getName().equals(username)) {
            return Response
                    .status(Response.Status.FORBIDDEN)
                    .build();
        }

        bcs.removeFromCollection(userId, bookId);

        return Response.
                status(Response.Status.OK)
                .entity("Book deleted successfully from collection!")
                .build();
    }

}
