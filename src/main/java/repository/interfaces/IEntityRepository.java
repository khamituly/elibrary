package repository.interfaces;

import java.sql.SQLException;
import java.util.LinkedList;

public interface IEntityRepository<T>{
    void add(T entity);
    void update(T entity);
    void remove(T entity) throws SQLException;
    LinkedList<T> query(String sql);
    T queryOne(String sql);
}

