package repository.interfaces;

import domain.User;
import domain.UserLoginData;

import java.sql.SQLException;
import java.util.LinkedList;

public interface IUserRepository extends IEntityRepository<User> {

    @Override
    default void add(User entity) {

    }

    @Override
    default void update(User entity) {

    }

    @Override
    default void remove(User entity) throws SQLException {

    }

    @Override
    default LinkedList<User> query(String sql) {
        return null;
    }

    @Override
    default User queryOne(String sql) {
        return null;
    }

    public User findUserByLogin(UserLoginData data);
    public User getUserByUsername(String username);
    public User getUserById(int id);
}
