package repository.interfaces;

import domain.Book;

import java.util.LinkedList;

public interface IBookRepository extends IEntityRepository<Book>{
    @Override
    default void add(Book entity) {

    }

    @Override
    default void update(Book entity) {

    }

    @Override
    default void remove(Book entity) {

    }

    @Override
    default LinkedList<Book> query(String sql) {
        return null;
    }

    @Override
    default Book queryOne(String sql) {
        return null;
    }

    public Book getBookById(int id);
}
