package repository.entity;

import domain.User;
import domain.UserLoginData;
import repository.db.PostgresRepository;
import repository.interfaces.IDBRepository;
import repository.interfaces.IUserRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class UserRepository implements IUserRepository {
    private IDBRepository dbrepo ;

    public UserRepository(){
        dbrepo = new PostgresRepository();
    }

    @Override
    public void add(User entity) {
        try {
            Statement stm = dbrepo.getConnection().createStatement();
            String sql = "INSERT INTO users(firstname,lastname,username,password)"+
                    "VALUES('"+entity.getFirstname()+"'," +
                            "'"+entity.getLastname()+"'," +
                            "'"+entity.getUsername()+"'," +
                            "'"+entity.getPassword()+"')";
            stm.execute(sql);
        } catch (SQLException ex){
            ex.printStackTrace();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void update(User entity) {
        String sql = "UPDATE users " +
                "SET ";
        int c = 0;
        if (entity.getFirstname() != null) {
            sql += "firstname =?, "; c++;
        }
        if (entity.getLastname() != null) {
            sql += "lastname = ?, "; c++;
        }
        if (entity.getPassword() != null) {
            sql += "password=?, "; c++;
        }

        sql = sql.substring(0, sql.length() - 2);

        sql += " WHERE username = ?";

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            if (entity.getFirstname() != null) {
                stmt.setString(i++, entity.getFirstname());
            }
            if (entity.getLastname() != null) {
                stmt.setString(i++, entity.getLastname());
            }
            if (entity.getPassword() != null) {
                stmt.setString(i++, entity.getPassword());
            }
            stmt.setString(i++, entity.getUsername());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }

    }

    @Override
    public void remove(User entity){

        try {
            String sql = "DELETE FROM users WHERE id = ?";
            PreparedStatement stmt = null;
            stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setInt(1,entity.getId());
            stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    public LinkedList<User> query(String sql) {
        try{
            Statement stm = dbrepo.getConnection().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            LinkedList<User> users = new LinkedList<>();
            while(rs.next()){
                User user = new User(
                        rs.getInt("id"),
                        rs.getString("firstname"),
                        rs.getString("lastname"),
                        rs.getString("username"),
                        rs.getString("password")
                );
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public User queryOne(String sql) {
        LinkedList<User> users = (LinkedList<User>)query(sql);
        return (users.isEmpty()? null : users.get(0));
    }

    @Override
    public User getUserById(int id) {
        String sql = "SELECT * FROM users WHERE id ="+id;
        return queryOne(sql);
    }

    @Override
    public User getUserByUsername(String username) {
        try {
            String sql = "SELECT * FROM users WHERE username = ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1, username);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return new User(
                        rs.getInt("id"),
                        rs.getString("firstname"),
                        rs.getString("lastname"),
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getString("role")
                );
            }
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }
        return null;
    }



    @Override
    public User findUserByLogin(UserLoginData data){
        try{
            String sql = "SELECT * FROM users WHERE username = ? AND password = ? LIMIT 1";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setString(1,data.getUsername());
            stmt.setString(2,data.getPassword());
            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                    return new User(
                            rs.getInt("id"),
                            rs.getString("firstname"),
                            rs.getString("lastname"),
                            rs.getString("username"),
                            rs.getString("password"),
                            rs.getString("role")
                );
            }

        } catch (SQLException e) {
            throw new BadRequestException("Can not run SQL statement"+ e.getMessage());
        }
        return null;
    }



}
