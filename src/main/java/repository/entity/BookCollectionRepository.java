package repository.entity;

import domain.Book;
import repository.db.PostgresRepository;
import repository.interfaces.IDBRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class BookCollectionRepository{
    private IDBRepository dbrepo;

    public BookCollectionRepository(){
        dbrepo = new PostgresRepository();
    }


    public void add(int userId, int bookId) {
        try {
            String sql = "INSERT INTO collections( userid,bookid) " +
                    "values(?,?)";
            PreparedStatement stm = dbrepo.getConnection().prepareStatement(sql);
            stm.setInt(1,userId);
            stm.setInt(2,bookId);
            stm.execute();
        } catch (SQLException ex){
            ex.printStackTrace();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void remove(int userId, int bookId) {

        try {
            String sql = "DELETE FROM collections WHERE userid = ? AND bookid= ?";
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setInt(1, userId);
            stmt.setInt(2, bookId);
            stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public LinkedList<Book> query(String sql) {
        try{
            Statement stm = dbrepo.getConnection().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            LinkedList<Book> books = new LinkedList<>();
            while(rs.next()){
                Book book = new Book(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("author")
                );
                books.add(book);
            }
            return books;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    public LinkedList<Book> getlistOfBook(int userId) {
        String sql = "SELECT books.id,books.name, books.author\n" +
                "FROM books,collections where books.id = collections.bookId and collections.userId ="+userId;
        LinkedList<Book> listOfBooks = (LinkedList<Book>)query(sql);
        return (listOfBooks.isEmpty() ? null : listOfBooks);
        }

}
