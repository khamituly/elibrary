package repository.entity;

import domain.Book;
import repository.db.PostgresRepository;
import repository.interfaces.IBookRepository;
import repository.interfaces.IDBRepository;

import javax.ws.rs.BadRequestException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

public class BookRepository implements IBookRepository {
    private IDBRepository dbrepo;

    public BookRepository() {
        dbrepo = new PostgresRepository();
    }


    @Override
    public void add(Book entity) {
        try {
            Statement stm = dbrepo.getConnection().createStatement();
            String sql = "INSERT INTO books(name,author)" +
                    "VALUES('" + entity.getName() + "'," +
                    "'" + entity.getAuthor() + "')";
            stm.execute(sql);
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void update(Book entity) {
        String sql = "UPDATE books " +
                "SET ";
        int c = 0;
        if (entity.getName() != null) {
            sql += "name = ?, ";
            c++;
        }
        if (entity.getAuthor() != null) {
            sql += "author = ?, ";
            c++;
        }

        sql = sql.substring(0, sql.length() - 2);

        sql += " WHERE id = ?";

        try {
            int i = 1;
            PreparedStatement stmt = dbrepo.getConnection().prepareStatement(sql);
            if (entity.getName() != null) {
                stmt.setString(i++, entity.getName());
            }
            if (entity.getAuthor() != null) {
                stmt.setString(i++, entity.getAuthor());
            }
            stmt.setInt(i++, entity.getId());

            stmt.execute();
        } catch (SQLException ex) {
            throw new BadRequestException("Cannot run SQL statement: " + ex.getMessage());
        }

    }

    @Override
    public void remove(Book entity) {
        try {
            String sql = "DELETE FROM books WHERE id = ?";
            PreparedStatement stmt = null;
            stmt = dbrepo.getConnection().prepareStatement(sql);
            stmt.setInt(1, entity.getId());
            stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public LinkedList<Book> query(String sql) {
        try {
            Statement stm = dbrepo.getConnection().createStatement();
            ResultSet rs = stm.executeQuery(sql);
            LinkedList<Book> books = new LinkedList<>();
            while (rs.next()) {
                Book book = new Book(
                        rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("author")
                );
                books.add(book);
            }
            return books;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public Book queryOne(String sql) {
        LinkedList<Book> books = (LinkedList<Book>) query(sql);
        return (books.isEmpty() ? null : books.get(0));
    }

    @Override
    public Book getBookById(int id) {
        String sql = "SELECT * FROM books WHERE id=" + id;
        return queryOne(sql);
    }

}

