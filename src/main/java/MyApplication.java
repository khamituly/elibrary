
import Filters.AdminFilter;
import Filters.CorsFilter;
import Filters.customAnnotations.JWTTokenNeeded;
import Filters.customAnnotations.OnlyAdmin;
import controllers.AuthorizationController;
import controllers.BookCollectionController;
import controllers.BookController;
import controllers.UserController;
import Filters.SecurityFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/")
public class MyApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> hs = new HashSet<>();
        hs.add(MultiPartFeature.class);
        hs.add(UserController.class);
        hs.add(BookController.class);
        hs.add(BookCollectionController.class);
        hs.add(AuthorizationController.class);
        hs.add(SecurityFilter.class);
        hs.add(CorsFilter.class);
        hs.add(AdminFilter.class);
        hs.add(JWTTokenNeeded.class);
        hs.add(OnlyAdmin.class);
        return hs;
    }

}

