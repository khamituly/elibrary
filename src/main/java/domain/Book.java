package domain;

public class Book {
    private int id;
    private String name;
    private String author;

    public Book(){}

    public Book(int id, String name, String author){
        setId(id);
        setName(name);
        setAuthor(author);
    }

    public Book(String name, String author){
        setName(name);
        setAuthor(author);
    }


    public void setId(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAuthor(String author){
        this.author = author;
    }

    public int getId(){
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getName() {
        return name;
    }
}
