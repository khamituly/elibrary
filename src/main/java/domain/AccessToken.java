package domain;

public class AccessToken {
    private String token;


    public AccessToken(String issueToken) {
        this.token = issueToken;
    }

    public String getToken(){
        return token;
    }
}
