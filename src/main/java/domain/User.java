package domain;



public class User {
    private int id;
    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private String role;

    public User(){}

    public User(int id, String firstname, String lastname, String username, String password,String role){
        setId(id);
        setFirstname(firstname);
        setLastname(lastname);
        setUsername(username);
        setPassword(password);
        setRole(role);
    }

    public User(String firstname, String lastname, String username, String password,String role) {
        setFirstname(firstname);
        setLastname(lastname);
        setUsername(username);
        setPassword(password);
        setRole(role);
    }

    public User(int id,String firstname, String lastname, String username,String role){
        setId(id);
        setFirstname(firstname);
        setLastname(lastname);
        setUsername(username);
        setRole(role);
    }

    public void setId(int id){
        this.id = id;
    }

    public void setFirstname(String firstname){
        this.firstname= firstname;
    }

    public void setLastname(String lastname){
        this.lastname = lastname;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public int getId(){
        return id;
    }

    public String getFirstname(){
        return firstname;
    }

    public String getLastname(){
        return lastname;
    }

    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + firstname + '\'' +
                ", surname='" + lastname + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

}
